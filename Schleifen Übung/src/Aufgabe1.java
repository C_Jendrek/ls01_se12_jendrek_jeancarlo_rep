/**
 * 
 */

/**
 * @author carlojendrek
 *
 */
public class Aufgabe1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Aufgabe 1
				//Schleife wird so lange ausgeführt bis der Wert 100 erreicht wird.
				
				int i =1;
				while (i<101) {
				System.out.print(i + " ");
				if(i%2 == 0) {
					System.out.println("ist durch 2 teilbar");
				} else  {
					System.out.println("ist nicht durch 2 teilbar");
				}
				i++;
				if (i==101) continue; 
				

			}
		
		
		
	}

}
