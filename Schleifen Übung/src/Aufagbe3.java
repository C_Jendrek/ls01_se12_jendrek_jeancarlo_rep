import java.util.Scanner;
/**
 * 
 */

/**
 * @author carlojendrek
 * Schleifen Aufgabe 3, Teiler einer Zahl
 * v0.1 vom 16.03.22
 */
public class Aufagbe3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		//Deklaration
		Scanner scan1 = new Scanner(System.in);
		
		
		System.out.println("Gebe eine Zahl ein: ");
		int zahl = scan1.nextInt();
		
		for (int a = 1; a < zahl; a++) {
			if (zahl % a == 0) {
				System.out.println("eingegebene Zahl ist durch  " + a + " teilbar."); 
			}
		}
		
		
	}

}
