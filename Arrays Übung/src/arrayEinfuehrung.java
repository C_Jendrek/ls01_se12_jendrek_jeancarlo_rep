
import java.util.Scanner;
public class arrayEinfuehrung {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    int intArray [] = {10,20,30,40,50};
    int eingabe;
    
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    
    intArray [2] = 1000;
    intArray [4] = 500;
    
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // Länge 3, mit 3 double Werten
    
    int doubleArray [] = {11,22,33};
    
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    System.out.println(doubleArray[1]);
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    System.out.println("An welchen Index soll der neue Wert?");
    eingabe = sc.nextInt();

    System.out.println("Geben Sie den neuen Wert f�r index " + eingabe + " an:");
    doubleArray[eingabe -1] = sc.nextInt();
    
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zunächst an, welche Werte Sie in der Ausgabe erwarten: 
    // 10,20,1000,500,50  
    
    for (int i = 0; i < intArray.length;i++) {
    	System.out.println(intArray[i]);
    }
    
    
    //Der intArray soll mit neuen Werten gefüllt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    
    
    
    //Der intArray soll mit neuen Werten gefüllt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    
    
    
    //Der intArray soll mit neuen Werten gefüllt werden
           //3. Felder automatisch mit folgenden Zahlen füllen: 10,20,30,40,50
    
    
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung
