import java.lang.reflect.Array;
import java.util.Scanner;
public class klausurVorbereitung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 Deklarieren Sie einen Array für 10 numerische Werte (Datentyp nach Wahl)

		 - Lassen Sie den Nutzer beliebige Werte eingeben

		 - Multiplizieren Sie alle Werte mit 100

		 - Addieren Sie auf                   Index 0 eine 5
		                                                Index 1 eine 10
		                                                Index 2 eine 15

		                                                ……..
		                                                Index 9 eine 50        Mit einer Schleife !!!

		 -  Geben Sie Ihr Array von hinten nach vorne aus

		 - Tauschen Sie die Elemente an der 1. und der letzten Indexposition (nutzen Sie dabei die Funktion length!)
	*/

		//Deklaration für ein Array mit 10 Numerischen Werten
		int[] myArray = new int[10];
		
		//Der Nutzer gibt 10 beliebige Werte ein
		Scanner sc = new Scanner (System.in);
		System.out.println("Bitte 10 beliebige Werte eingeben.");
		for (int i = 0;i < myArray.length;i++) {
			myArray[i] = sc.nextInt();
		}
		
		//Alle Werte werden mit 100 multipliziert
		for (int i = 0; i < myArray.length;i++) {
		myArray[i] = myArray[i]*100;
		}
		
		//Index 0 - 9 wird mit 5 - 50 auf addiert
		for (int i = 0; i < myArray.length;i++) {
			int wert = (i + 1) * 5;
			myArray[i] = myArray[i] + wert;
		}
		
		//Das Array wird von hinten nach vorne ausgegeben
		for (int i = myArray.length -1; i >= 0; i--) {
		System.out.println(myArray[i]);
		}
		
		//Die erste und Letzte Index-Postion wird getauscht und anschließend ausgegeben
		int variable;
		variable = myArray[0];
		myArray[0] = myArray[myArray.length -1];
		myArray[myArray.length -1] = variable;
		
		for (int i = 0; i < myArray.length; i++) {
			System.out.println(myArray[i]);
		}

		
	}

}
