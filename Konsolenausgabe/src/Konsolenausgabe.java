
public class Konsolenausgabe {
	public static void main(String[] args) {
	// Aufgabe 1
		int temperatur = 27;
		String wann = "heute";
		double zahl = 22.4234234;

		
		
	System.out.println("Das Wetter heute ist gut.\n");
	System.out.println("Gut ist das Wetter heute.");
	
	System.out.println("Das Wetter ist " + wann + temperatur +  " Grad.");
	
	// Beim print wird der Text in einer Zeile ausgegeben, bei println in einer Zeile darunter.
	
	// Aufgabe 2

    System.out.println(" ");
	System.out.println("   **");
	System.out.println("   **");
	System.out.println("  ****");
	System.out.println(" ******");
	System.out.println("********");
	System.out.println("  ***");
	System.out.println("  ***");
	
	//Aufgabe 3
	
	System.out.printf(" %.2f", zahl);
	}
}
